
<?php
require("connect_bd.php");
require("navbar.php");
?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="page_liste3.php">
                  Liste complète
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="page_tri.php">
                  Trier <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_ajouter.php">
                  Ajouter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_supprimer.php">
                  Supprimer
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="">
            <!-- class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom" -->
            <h1 class="h2">Trier</h1><br>
            <form class="" action="page_tri.php" method="GET">
              <div class="form-group">
                <div class="btn-group btn-group" data-toggle="buttons">
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="NomJeu"> Nom
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="NomDev"> Développeur
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="Annee"> Année
                  </label>
                  <label class="btn btn-secondary">
                    <input type="radio" name="options" autocomplete="off" value="Genre"> Genre
                  </label>
                </div>
              </div>

              <?php
              if ((!isset($_GET['options']))) {
                echo "<p class=\"warning_liste\">Veuillez sélectionner une catégorie </p>";
              ?>
              <button type="submit" class="btn btn-outline-primary btn-block">Trier</button>
              <hr class="mb-4">
              <?php
              }
              else {
                ?>
                <br>
                <button type="submit" class="btn btn-outline-primary btn-block">Trier</button>
                <hr class="mb-4">
                <?php
                if ($_GET['options'] == "NomDev") {
                  $x = "Développeur";
                }
                elseif ($_GET['options'] == "NomJeu") {
                  $x = "Nom";
                }
                elseif ($_GET['options'] == "Annee") {
                  $x = "Année";
                }
                elseif ($_GET['options'] == "Genre") {
                  $x = "Genre";
                }
                echo "<p class = \"titre_liste\">Liste triée par ".$x."</h2>";
                ?>
                <!-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> -->
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th scope="col">Nom</th>
                      <th scope="col" class="">Développeur</th>
                      <th scope="col">Genre</th>
                      <th scope="col" class="cell1">Année</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $sql = "select * from JEUXVIDEO natural join DEVELOPPEUR natural join CREER order by ".$_GET['options'];
                    if(!$connexion->query($sql)) echo "Pb d'accès au CARNET";
                    else {
                      foreach ($connexion->query($sql) as $row)
                      echo "
                      <tr>
                      <td class = \"c\"><a href=\"test.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>
                      <td class = \"l\">".$row['NomDev']."</td>
                      <td>".$row['Genre']."</td>
                      <td>".$row['Annee']."</td>
                      </tr>
                      ";
                    }
                    ?>
                  <!-- <hr class="mb-4"> -->
                <?php } ?>
                </form>
              </div><br>
            </tbody>
          </table>
        </main>
      </div>
    </div>
  </body>
</html>
