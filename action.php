<?php
require("connect_bd.php");
require("navbar.php");

   if (isset($_POST['suppr_nomJeu'])) {
     $n = $_POST['suppr_nomJeu'];
     $a = "supprimé";

     $sql3="DELETE from CREER where NomJeu =:supprnomJeu";
     $sql1="DELETE from JEUXVIDEO where NomJeu =:supprnomJeu";

     $stmt3=$connexion->prepare($sql3);
     $stmt3->bindParam(':supprnomJeu', $_POST['suppr_nomJeu']);
     $stmt3->execute();

     $stmt1=$connexion->prepare($sql1);
     $stmt1->bindParam(':supprnomJeu', $_POST['suppr_nomJeu']);
     $stmt1->execute();

     ?>
     <main role="main" class="container"><br><br>
       <div class="starter-template">
         <div class="alert alert-success" role="alert">
           <h4 class="alert-heading">Supprimé !</h4>
           <p>Le jeu <?php echo $n ?> a bien été <?php echo $a ?></p>
           <a href="page_liste3.php" class="btn btn-secondary my-2">Retour à la liste</a>
         </div>
        </div>
      </main>
      <?php
   }

   else {
     $n = $_POST['nom'];
     $a = "ajouté";
     $sql4="INSERT into JEUXVIDEO values (:nom,:genre,:nomimage)";
     $sql5="INSERT IGNORE into DEVELOPPEUR values (:dev)";
     $sql6="INSERT into CREER values (:nom, :dev, :annee)";

     $stmt4=$connexion->prepare($sql4);
     $stmt4->bindParam(':nom', $_POST['nom']);
     $stmt4->bindParam(':genre', $_POST['genre']);
     $stmt4->bindParam(':nomimage', $_POST['nomimage']);


     $stmt5=$connexion->prepare($sql5);
     $stmt5->bindParam(':dev', $_POST['dev']);


     $stmt6=$connexion->prepare($sql6);
     $stmt6->bindParam(':nom', $_POST['nom']);
     $stmt6->bindParam(':dev', $_POST['dev']);
     $stmt6->bindParam(':annee', $_POST['annee']);
     if (!$stmt4->execute() || !$stmt5->execute() || !$stmt6->execute()) {
      ?>
      <main role="main" class="container"><br><br>
        <div class="starter-template">
          <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">ERREUR3 !</h4>
            <p>Le jeu <?php echo $n ?> n'a pas été <?php echo $a ?></p>
            <a href="page_ajouter.php" class="btn btn-secondary my-2">Retour</a>
          </div>
          </div>
        </main>
      <?php
     }
     else {
       ?>
       <main role="main" class="container"><br><br>
         <div class="starter-template">
           <div class="alert alert-success" role="alert">
             <h4 class="alert-heading">Ajouté !</h4>
             <p>Le jeu <?php echo $n ?> a bien été <?php echo $a ?></p>
             <a href="page_liste3.php" class="btn btn-secondary my-2">Retour à la liste</a>
           </div>
          </div>
        </main>
       <?php
     }
     // echo "insert into JEUXVIDEO values ("."'".$_POST['nom']."',"."'".$_POST['dev']."',"."'".$_POST['annee']."',"."'".$_POST['genre']."');";
   }
?>

  </body>
</html>
