<?php
require("connect_bd.php");
require("navbar.php");
?>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <!-- class = colonne -->
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="page_liste3.php">
              Liste complète <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_tri.php">
              Trier
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="page_ajouter.php">
              Ajouter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="page_supprimer.php">
              Supprimer
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <h4 class="h2">Supprimer un jeu</h4><br>
          <form class="needs-validation" action="action.php" method="post">
            <!--  novalidate-->
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Nom du jeu</label>
                <select id="inputState" class="form-control" name="suppr_nomJeu">
                  <?php
                  $sql = "select * from CREER";
                  if(!$connexion->query($sql)) echo "Pb d'accès à la base";
                  else {
                    foreach ($connexion->query($sql) as $row)
                    echo "<option>".$row['NomJeu']."</option>";
                        }
                  ?>
                </select>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-outline-primary btn-block" type="submit">Confirmer</button>
          </form><br>
        </main>
      </div>
    </div>
  </body>
</html>
