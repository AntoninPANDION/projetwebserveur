<?php
require("connect_bd.php");
require("navbar.php");

?>



    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="page_liste3.php">
                  Liste complète
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_tri.php">
                  Trier <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_ajouter.php">
                  Ajouter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_supprimer.php">
                  Supprimer
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="">
            <h1 class="h2"><?php echo "".$_GET['nom_page']; ?></h1><br>
            <?php
              $sql = "select * from JEUXVIDEO natural join DEVELOPPEUR natural join CREER where NomJeu = \"".$_GET['nom_page']."\"";
              if(!$connexion->query($sql)) echo "Pb d'accès au CARNET";
              else {
                foreach ($connexion->query($sql) as $row)
            ?>
            <p><?php echo "".$row['NomJeu'] ?> est un jeu de type <?php echo "".$row['Genre'] ?> développé par <?php echo "".$row['NomDev'] ?> et sorti en <?php echo "".$row['Annee'] ?>.</p>
            <p><img src=<?php echo "pictures/".$row['Image'] ?> alt="">
            <?php
              }
            ?>
          </div>
        </main>
      </div>
    </div>
  </body>
</html>
