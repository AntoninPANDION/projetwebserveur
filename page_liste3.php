
<?php
require("connect_bd.php");
require("navbar.php");
?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <!-- class = colonne -->
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="page_liste3.php">
                  Liste complète <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_tri.php">
                  Trier
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_ajouter.php">
                  Ajouter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="page_supprimer.php">
                  Supprimer
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <!-- class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom" -->
          <h1 class="h2">Liste complète</h1><br>
          <div class="table-responsive tableau">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th scope="col">Nom</th>
                  <th scope="col" class="">Developpeur</th>
                  <th scope="col">Genre</th>
                  <th scope="col" class="cell1">Année</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $sql = "select * from JEUXVIDEO natural join DEVELOPPEUR natural join CREER";
                if(!$connexion->query($sql)) echo "Pb d'accès à la base";
                else {
                  foreach ($connexion->query($sql) as $row)
                  echo "
                  <tr>
                    <td class = \"c\"><a href=\"test.php?nom_page=".$row['NomJeu']."\">".$row['NomJeu']."</a></td>
                    <td class = \"l\">".$row['NomDev']."</td>
                    <td>".$row['Genre']."</td>
                    <td>".$row['Annee']."</td>
                  </tr>
                  ";
                }
                ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>
  </body>
</html>
