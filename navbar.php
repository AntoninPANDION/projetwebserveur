<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Site Jeux Vidéo</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style4.css" rel="stylesheet">
  </head>

  <body class="bodyOPAQUE">
    <nav class="navbar navbar-expand-md navbar-light navbar-bg fixed-top flex-md-nowrap shadow">
          <a class="navbar-brand" href="squelette.php">
            <img src="icon/gamepad-console_icon.png" width="30" height="30" alt="">
          </a>
          <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="squelette.php">Accueil <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="page_liste3.php">Liste des Jeux</a>
              </li>
            </ul>
          </div>
    </nav>
